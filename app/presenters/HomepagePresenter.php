<?php

namespace App\Presenters;

use Nette,
	App\Model;


/**
 * Homepage presenter.
 */
class HomepagePresenter extends BasePresenter
{

	public function renderDefault()
	{
		$this->template->anyVariable = 'any value';
		$this->template->article = $this->getArticle();
	}
	
	public function renderArticle() {
		$this->template->post = $this->getArticle();
	}
	
	private function getArticle(){
		return \Nette\Utils\ArrayHash::from([
			'headline' => 'Smrt obchodniho cestujiciho',
			'text' => 'text clanku text clanku text clanku text clanku text clanku text clanku text clanku text clanku text clanku text clanku text clanku text clanku text clanku text clanku text clanku text clanku text clanku text clanku text clanku '
		]);
	}
	
	public function createComponentForm() {
		$form = new \Nette\Application\UI\Form;
		$form->addText('x', 'X');
		return $form;
	}

}
